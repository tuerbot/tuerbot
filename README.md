# tuerbot by Daniel & Peter

Der Tuerbot meldet sich via Telegram mit einer freundlichen Nachricht und einem Bild von einer MJPEG Kamera an der Tür

## Funktionen

* Sendet ein Foto mit einer netten Nachricht bei jedem klingeln
* nutzt die Telegram-Bot API
* Selbst auf einem Raspberry Pi Zero W so schnell, dass Delays für die Kameras eingebaut werden mussten :)
* Open Source
* Kann aus Telegram bedient werden


## Installation

Einrichtung auf einem Raspberry Pi:

```
$ sudo -s
# apt update
# apt install git python python3 python-pip screen
# pip install pip --upgrade
# pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U
# pip install telepot --upgrade
# exit
$ cd ~
$ git clone https://git.rippen-ks.de/Peter/tuerbot tuerbot
$ cd tuerbot
$ cp credentialsEXAMPLE.py ../credentials.py
$ cp init-d-script /etc/init.d/tuerbot-init
$ sudo chmod 755 /etc/init.d/tuerbot-init
$ sudo chmod +x /etc/init.d/tuerbot-init
```

## Nutzung in der Shell

Tuerbot starten, stoppen, neustarten:
```
$ sudo /etc/init.d/tuerbot-init start
$ sudo /etc/init.d/tuerbot-init stop
$ sudo /etc/init.d/tuerbot-init restart
```

## Nutzung unter Telegram

Kommando  | Wirkung
------------- | -------------
/pic  | Zeigt das letzte Bild von der Klingeltaste an
/pic2  | Schießt ein neues Bild, sendet es danach sofort zu
/chatid  | Zeigt die eigene Chat-ID an
/reportchatid  | Sendet die eigene Chat-ID mit dem Vornamen an den/die Admin(s)

## Automatisches verschieben der images auf ein NAS via SMB

```
$ cp mv-tuerbot-images.sh /opt/
$ sudo chmod 755 /opt/mv-tuerbot-images.sh
$ sudo chmod +x /opt/mv-tuerbot-images.sh
$ sudo apt install cifs-utils
```

/etc/fstab
```
## SMB Verbindung zu einer Windows Server 2016 SMB-Freigabe
//192.168.0.10/tuerbot-images-archiv /opt/tuerbot-images-archiv cifs username=NUTZERNAME,passwd=PASSWORT,uid=1001,gid=1001,file_mode=0777,dir_mode=0777 0 0
```

Wir verschieben die images jeden Samstag und nutzen dafür einen Cronjob:

$ sudo crontab -e
```
0 1 * * 6 bash /opt/mv-tuerbot-images.sh >/dev/null 2>&1
```
