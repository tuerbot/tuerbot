#!/usr/bin/python
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import time
import sys
import os
import telepot
import thread
import subprocess

#going to home directory
sys.path.insert(0, '/home/pi/')
#importing credentials from home directory
from credentials import *

bot = telepot.Bot(botToken)

SWITCH_1 = 17
DELAY = 0.01


def setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(SWITCH_1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    os.chdir('/home/pi/tuerbot')


def destroy():
    GPIO.cleanup()
    sys.exit()

def sendError(chatid):
    bot.sendMessage(chatid, 'Es hat jemand geklingelt, jedoch gab es einen Fehler beim machen des Bildes.')

def sendPicture(chatid):
    bot.sendPhoto(chatid, open('../images/image-latest.jpg', 'rb'),caption="Jemand hat geklingelt :)")


def loop():
    while True:
        input_value = GPIO.input(SWITCH_1)
        if input_value == True:
            time.sleep(DELAY)
        else:

	    command = 'bash capture.sh ' + camURL + " " + camUser + " " + camPass
            capture_output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)

            error = True
            for line in capture_output.strip().split('\n'):
                if '200 OK' in line:
                    error = False
            if error == False:
         	    for chatid in chat_ids:
                     thread.start_new_thread(sendPicture, (chatid,))
           	    os.system('./savepic.sh')
            else:
         	    for chatid in chat_ids:
                     thread.start_new_thread(sendError, (chatid,))
	    time.sleep(5)


if __name__ == '__main__':
    setup()
    try:
        loop()
    except KeyboardInterrupt:
        destroy()
