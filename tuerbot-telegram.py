#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import telepot
import sys
import os
import random
import datetime
import subprocess

#going to home directory
sys.path.insert(0, '/home/pi')
#importing credentials from home directory
from credentials import *

os.chdir('/home/pi/tuerbot')


def handle(msg):
    chat_id = msg['chat']['id']
    command = msg['text']
    vorname = msg['chat']['first_name']
    print(msg)

    print('Got command: %s' % command)

    if command == '/pic2':
        for chatid in chat_ids:
            if chat_id == chatid:
		command = 'bash pic2.sh ' + camURL + " "  + camUser + " " + camPass
		print(command)
                pic2_output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
                error = True
                for line in pic2_output.strip().split('\n'):
		    print(line)
                    if '200 OK' in line:
                        error = False
                if error == False:
                    bot.sendPhoto(chat_id, open('../images/image-latest-pic2.jpg', 'rb'),caption="Angefragtes Bild \"pic2\"")
	            os.system('./savepic2.sh')
                else:
                    bot.sendMessage(chat_id, "Es gab einen Fehler beim machen des Bildes!!")
    elif command == '/pic':
        for chatid in chat_ids:
            if chat_id == chatid:
                bot.sendPhoto(chat_id, open('../images/image-latest.jpg', 'rb'),caption="Angefragtes Bild \"pic\"")
    elif command == '/chatid':
        toSend = ""
        for chatid in chat_ids:
            if chat_id == chatid:
                toSend += "Du bist verifiziert :)"
        toSend += "\n" + str(chat_id)
        bot.sendMessage(chat_id, toSend)
    elif command == '/reportchatid':
        if (len(admins) == 1):
            bot.sendMessage(chat_id, 'Deine Chat-ID wurde an den Admin gesendet.')
        else:
            bot.sendMessage(chat_id, 'Deine Chat-ID wurde an die Admins gesendet.')
        for admin in admins:
            bot.sendMessage(admin, str(vorname) + ' hat eine neue Chat-ID angefragt:\n' + str(chat_id))
    elif command == '/test':
        time.sleep(2)
        bot.sendMessage(chat_id, 'Dies ist ein Test.')
    elif command == "/start":
        bot.sendMessage(chat_id, "Heyy, ich bin ein Tuerbot. Ich sage bescheid, wenn jemand klingelt. \n\nPruefe mit '/chatid' ob du verifiziert bist. \nWenn nicht, kannst du mit '/reportchatid' eine Verifizierung anfragen. \n\nViel Spass!")
    elif command.lower() == "savage":
        bot.sendMessage(chat_id, "Fuckin right")
    elif command.lower() == "hallo":
        bot.sendMessage(chat_id, "Hallo :)")
    else:
        bot.sendMessage(chat_id, "Ich hab's nicht verstanden, bitte gib mir ein Kommando, den ich verstehe :) \nDanke.")


bot = telepot.Bot(botToken)
bot.message_loop(handle)

while 1:
    time.sleep(10)
